module Main where

import Control.Monad.Except (runExcept)
import Data.Either (hush)
import Data.Maybe (Maybe(..), fromMaybe)
import EC.Flow (startECFlow)
import Effect.Aff (Aff, launchAff_)
import Logger (logDOM)
import Prelude (Unit, bind, pure, ($))

import EC.Types (ECPayload(..), SDKResponse, TxnPayload(..))
import Effect (Effect)
import Foreign (Foreign)
import Foreign.Class (decode)

getDummyECPayload :: ECPayload
getDummyECPayload = Txn $ TxnPayload $ { action: "upiTxn",
  custVpa: Just "9671102079@ybl",
  displayNote: Just "",
  endUrls: Just [""],
  handlePolling: Just false,
  merchantKeyId: Just "2980",
  orderDetails: Just "{\"order_id\":\"75474\",\"timestamp\":\"1605862259982\",\"first_name\":\"bhawesh\",\"last_name\":\"agarwal\",\"amount\":\"1.00\",\"customer_id\":\"1234567890\",\"customer_phone\":\"8077585960\",\"customer_email\":\"test@gmail.com\",\"udf1\":\"Delhi\",\"udf2\":\"Columbia\",\"return_url\":\"http://localhost:8083/merchant/end.html\",\"mandate_max_amount\":\"100.0\",\"metadata.PAYTM_V2:SUBSCRIPTION_EXPIRY_DATE\":\"2022-11-20\",\"metadata.PAYTM_V2:SUBSCRIPTION_FREQUENCY_UNIT\":\"MONTH\",\"metadata.PAYTM_V2:SUBSCRIPTION_FREQUENCY\":\"1\",\"metadata.PAYTM_V2:SUBSCRIPTION_START_DATE\":\"2020-11-20\",\"metadata.PAYTM_V2:SUBSCRIPTION_GRACE_DAYS\":\"5\",\"merchant_id\":\"idea_preprod\",\"metadata.JUSPAY:gateway_reference_id\":\"vodafone1\"}",
  orderId: "75474",
  paymentMethod: Just "UPI",
  saveToLocker: Just false,
  signature: Just "rjJRhwD6DPwDW3pBayrBKWh7+EDcMoOy4M34odf97ewteV1XIOgPSOB+I2qupXVAp7KYyYZQXjpHYPGbLIbPK/uVPBohCyfoARMQ/GrEWoG1fLh0tlkOUEK3dKExkqsDH6ySbDDXQleqAk0p0dCuXe9DHUISz7+sszayF1hTzgdoklMweGfHPBV2XGarPb/ATCCAQ7h3eDquLgaCojQDAaBFRz9pOjlVNcIhm5zSiEfrq35QjbySCgKluKWnAHWUI7SKIXbS1WgsLGi6vHSGt/hGadsLh4DQIcv8UtoZEq11+YduTqIiCaU+WCf3S/zOs/b/XuCcC2XScyQWRa0Htw==",
  upiSdkPresent: Just false,
  payWithApp : Nothing,
  currency : Nothing,
  getAvailableApps : Nothing,
  showLoader : Nothing
}

makeTxn :: Foreign -> Aff (Maybe SDKResponse)
makeTxn payload' = do
  payload <- pure $ hush $ runExcept $ decode payload'
  startECFlow (fromMaybe getDummyECPayload payload)

handle :: Foreign -> Effect Unit
handle sdkPayload = launchAff_ $ do
  response <- makeTxn sdkPayload
  _ <- pure $ logDOM "internal" "Existing hyper-widget.." ""
  pure $ response