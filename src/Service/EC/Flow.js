exports["startECFlow'"] = function (payload) {
    console.log(payload);
    payload = JSON.parse(payload);
    console.log(payload);

    return function (callback) {
        return function () {
            var success = function (code) {
                return function (result) {
                    return function () {
                        result = JSON.parse(result);
                        window.logger("external")("Got SDK Response from EC-DUI through callback function")(result);
                        window.logger("internal")("Fetching response from SDK response...")("");
                        result = result.payload;
                        window.logger("internal")("Response fetched successfully!!")(result);
                        result = JSON.stringify(result);
                        callback(result)();
                    }
                }
            }

            if (window.JOS) {
                payload.service_based = "true";
                window.logger("external")("Sending SDK Payload to EC-DUI through JOS using onMerchantEvent(process)")("");
                window.JOS.emitEventWithLog('in.juspay.ec')("onMerchantEvent")(["process", JSON.stringify(payload)])(success)();

            } else {
                console.error("JOS not present")
                callback("FAIL")();
            }
        }
    }
}