module EC.Flow where

import Prelude (Unit, bind, pure, ($), (*>), (>>>))
import EC.Types (ECPayload, SDKPayload(..), SDKResponse)
import Effect.Aff (Aff, makeAff, nonCanceler)
import Effect (Effect)
import Data.Maybe (Maybe)
import Foreign.Generic (encodeJSON, decodeJSON)
import Data.Either (Either(..), hush)
import Control.Monad.Except (runExcept)
import Logger (logDOM)

type AffSuccess s = (s -> Effect Unit)

foreign import startECFlow' :: MicroAPPInvokeSignature

type MicroAPPInvokeSignature = String -> (AffSuccess String) -> Effect Unit

startECFlow :: ECPayload -> Aff (Maybe SDKResponse)
startECFlow ecPayload = do
  _ <- pure $ logDOM "internal" "Creating SDK payload from payload for EC-DUI..." ecPayload
  sdkPayload <- getSDKPayload ecPayload
  _ <- pure $ logDOM "internal" "SDK Payload created successfully" sdkPayload
  response <- makeAff (\cb -> (startECFlow' (encodeJSON sdkPayload) (Right >>> cb) )*> pure nonCanceler)
  pure $ hush $ runExcept $ decodeJSON response


getSDKPayload :: ECPayload -> Aff SDKPayload
getSDKPayload payload = do
  let service = "in.juspay.ec"
  pure $ SDKPayload { service, requestId: "1752438e-dda1-49aa-834f-a2defa64f2ae", payload: payload, client_id : "", customer_id: "1234567890", environment: "sandbox", merchant_id: "idea_preprod" }
