module EC.Types where
  
import Foreign.Class (class Decode, class Encode)
import Foreign.Generic (genericEncode, genericDecode, defaultOptions)
import Data.Generic.Rep (class Generic)
import Data.Maybe

newtype SDKPayload
  = SDKPayload
  { requestId :: String
  , payload :: ECPayload
  , service :: String
  , merchant_id :: String
  , client_id :: String
  , customer_id :: String
  , environment :: String
  }
derive instance genericSDKPayload :: Generic SDKPayload _
instance sdkPayloadEncode :: Encode SDKPayload where encode = (genericEncode (defaultOptions { unwrapSingleConstructors = true }))

data ECPayload
  = Txn TxnPayload

derive instance genericECPayload :: Generic ECPayload _
instance eCPayloadEncode :: Encode ECPayload where encode = (genericEncode (defaultOptions { unwrapSingleConstructors = true }))
instance eCPayloadDecode :: Decode ECPayload where decode = (genericDecode (defaultOptions { unwrapSingleConstructors = true }))

newtype TxnPayload = TxnPayload 	{ action :: String
    , endUrls :: Maybe (Array String)
    , orderId :: String
    , upiSdkPresent :: Maybe Boolean
    , paymentMethod :: Maybe String
    , displayNote :: Maybe String
    , custVpa :: Maybe String
    , payWithApp :: Maybe String
    , currency :: Maybe String
    , getAvailableApps :: Maybe Boolean
    , signature :: Maybe String
    , merchantKeyId :: Maybe String
    , orderDetails :: Maybe String
    , saveToLocker :: Maybe Boolean
    , handlePolling :: Maybe Boolean
    , showLoader :: Maybe Boolean
    }

derive instance genericTxnPayload :: Generic TxnPayload _
instance txnPayloadEncode :: Encode TxnPayload where encode = (genericEncode (defaultOptions { unwrapSingleConstructors = true }))
instance txnPayloadDecode :: Decode TxnPayload where decode = (genericDecode (defaultOptions { unwrapSingleConstructors = true }))

newtype SDKResponse =
  SDKResponse
    {
      service :: String
    , payload :: String
    , errorMessage :: String
    , errorCode :: String
    , error :: Boolean
    }
derive instance genericSDKResponse :: Generic SDKResponse _
instance sdkResponseDecode :: Decode SDKResponse where decode = (genericDecode (defaultOptions { unwrapSingleConstructors = true }))
