module Logger where

import Prelude (Unit)
import Effect (Effect) 

foreign import logDOM :: forall a. String -> String -> a -> Effect Unit