var purescript = require("./output/Main");

function guid() {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000);
    }
    return s4();
  }

window.logger("internal")("Initialising hyper-widget...")("");
var txnButton = document.getElementById("txnButton");

txnButton.disabled = true;

window.logger("internal")("hyper-widget initialized successfully!!")("");

var orderPayload = JSON.stringify({"order_id":guid().toString(),"timestamp":new Date().getTime().toString(),"first_name":"bhawesh","last_name":"agarwal","amount":"1.00","customer_id":"1234567890","customer_phone":"8077585960","customer_email":"test@gmail.com","udf1":"Delhi","udf2":"Columbia","return_url":"http://localhost:8083/merchant/end.html","mandate_max_amount":"100.0","metadata.PAYTM_V2:SUBSCRIPTION_EXPIRY_DATE":"2022-11-20","metadata.PAYTM_V2:SUBSCRIPTION_FREQUENCY_UNIT":"MONTH","metadata.PAYTM_V2:SUBSCRIPTION_FREQUENCY":"1","metadata.PAYTM_V2:SUBSCRIPTION_START_DATE":"2020-11-20","metadata.PAYTM_V2:SUBSCRIPTION_GRACE_DAYS":"5","merchant_id":"idea_preprod","metadata.JUSPAY:gateway_reference_id":"vodafone1"});

function generateSignature(orderPayload) {
    console.log("generating signature");
    window.logger("internal")("Generating signature...")("");

    return new Promise((resolve, reject) => {
      fetch(`api/sign-idea?payload=${orderPayload}`)
        .then((res) => {
          if (res.status === 200) {
            return res.text();
          }
          throw new Error();
        })
        .then((text) => resolve(text))
        .catch((err) => reject(err));
    });
}

generateSignature(orderPayload).then((signature) => {
    var payload =  {
        custVpa: "9671102079@ybl",
        displayNote: "",
        endUrls: [""],
        handlePolling: false,
        merchantKeyId: "2980",
        paymentMethod: "UPI",
        saveToLocker: false,
        upiSdkPresent: false,
        signature: encodeURIComponent(signature),
        orderId: JSON.parse(orderPayload).order_id,
        orderDetails: orderPayload,
    }

    window.logger("internal")("Signature generated successfully!! ")(signature);
    window.logger("internal")("Payload")(payload);
    txnButton.disabled = false;
    window.logger("internal")("Click on \"Make Txn\" button to do the transaction!!")("");

    txnButton.addEventListener("click", function (event) {
        purescript.handle(payload)();
    });
});