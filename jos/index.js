window.logContainerId = "main-logs";
require('../src/Logger.js');
window.logger("internal")("Main frame loaded!")("");
// JOS Configuration
window.configVersion = "1.0.0"
window.getConfig = function(){
    return {
        configVersion : window.configVersion,
        apps: {
            "in.juspay.hyperos": {
                "src": "",
                "version": "1.0rc1",
                "iconUrl" : "",
                "assets" : {
                    "config": "",
                    "manifest": ""
                },
                "root": "http://0.0.0.0:3000/hyperos-microapp.html",
                "scripts": [],
                "entry": "",
                "canOpen": ["in.juspay.ec"]
            },
            "in.juspay.ec": {
                "src": "",
                "version": "1.0rc1",
                "iconUrl" : "",
                "assets" : {
                    "config": "",
                    "manifest": ""
                },
                "root": "http://0.0.0.0:3000/ec-microapp.html",
                "scripts": [],
                "entry": "",
                "canOpen": []
            }
        }
    }
}

window.Analytics = {
    trackMicroAppVerison: function() {
        return function() {}
    },
    trackEventInfo: function() {
        return function() {
            return function() {
            
            }
        }
    },
    _trackContext: function() {
        return function() {
            return function() {
                return function() {
                    return function() {
                  
                    }
                }
            }
        }
    },
    trackExceptionCritical: function() {
        return function() {
            return function() {
            
            }
        }
    }
}

window.getTrackerModule = {
    Main: {
        initTracker: function() {
            return window.Analytics
        }
    }
}

window.__BOOT_LOADER = {}

require("regenerator-runtime/runtime");

window.prestoUI = require("./presto-ui");

require("./boot_loader");

payload = {
    service : "in.juspay.hyperos",
    pre_fetch : "false",
    betaAssets : false,
    service_based : false,
    services : ["in.juspay.hyperos"],
    requestId : ""    
}

var innerPayload = {
    clientId : ""
    , merchantId : "idea_preprod"
    , environment : "sandbox"
    , customerId : "1234567890"
    , action : "initiate"
}

Object.assign(payload, window.__payload, {payload: innerPayload});

window.logger("internal")("Creating microapp iframes using JOS....!")("");
window.onMerchantEvent("initiate", JSON.stringify(payload), function() {});
window.logger("internal")("Iframes created successfully!")("");

//Middle layer to capture the JOS emit event function in middle for logs
iframeWindow = document.getElementById('in.juspay.hyperos').contentWindow;
iframeWindow.JOS.emitEventWithLog = function (receiver) {
    return function (event) {
        return function (data) {
            return function (callback) {
                return function () {
                    window.logger("external")("Get JOS emitEvent")("");
                    window.logger("external")("Caller: in.juspay.hyperos (hyper-widget)")("");
                    window.logger("external")("Reciever: "+ receiver)("");
                    window.logger("external")("Event: " + event)("");
                    window.logger("external")("Event Name: " + data[0])("");
                    window.logger("external")("Payload: ")(data[1]);
                    iframeWindow.JOS.emitEvent(receiver)(event)(data)(callback)();
                }
            }
        }
    }
}