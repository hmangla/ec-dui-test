const transformCommonJS = require("convert-commonjs/src/transformCommonJS.js")
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require("path");
const webpack = require('webpack');

module.exports = {
    entry : {
        "jos": ["./jos/index.js"],
        "hyper-widget-sdk": ["./index.js"]
    },
    output: {
      path: __dirname + "/dist",
      filename: "[name].js",
      sourceMapFilename: "[name].map",
    },
    devServer: {
        host: "0.0.0.0",
        contentBase: path.join(__dirname, 'dist'), 
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
          "Access-Control-Allow-Headers": "X-Requested-With, content-type, Authorization",
          "X-Content-Type-Options": "Disabled"
        },
        proxy: {
            "/api": {
                target: "https://stormy-wave-80220.herokuapp.com",
                pathRewrite: {"^/api" : ""},
                secure: false,
                changeOrigin: true
            },
            "/sandbox": {
                target: "https://sandbox.juspay.in",
                pathRewrite: {"^/sandbox" : ""},
                secure: false,
                changeOrigin: true
            },
            "/juspayapi": {
              target: "https://api.juspay.in",
              pathRewrite: {"^/juspayapi" : ""},
              secure: false,
              changeOrigin: true
            },
            "/hyper": {
              target: "https://payments.juspay.in/hyper",
              pathRewrite: { "^/hyper": "" },
              secure: false,
              changeOrigin: true,
            },
        },
    },
    plugins: [
      new webpack.DefinePlugin({
        'window.__OS': JSON.stringify("WEB"),
      }),
      new HtmlWebpackPlugin({
        'template' : "./jos/jos.html",
        'inject' : false,
        filename : "index.html",
        'minify': false
      }),
      new HtmlWebpackPlugin({
        'template' : "./src/hyperos-microapp.html",
        'inject' : false,
        filename : "hyperos-microapp.html",
        'minify': false
      }),
      new HtmlWebpackPlugin({
        'template' : "./src/ec-microapp.html",
        'inject' : false,
        filename : "ec-microapp.html",
        'minify': false
      })
    ],
    module: {
        rules: [
          {
            test: /\.m?js$/,
            exclude: /(node_modules|bower_components)(?!\/presto-ui)/,
            use: {
              loader: 'babel-loader',
              options: {
                presets: [
                    [
                      '@babel/preset-env',
                      {
                        "targets": {
                          "chrome": "30",
                          "ie": "11"
                        }
                      }
                    ]
                ]
              }
            }
          },
          {
            test: /\.m?js$/,
            include: /(output)/,
            exclude: /(node_modules|bower_components)(?!\/presto-ui)/,
            use: {
              loader: 'babel-loader',
              options: {
                presets: ['@babel/preset-env'],
                plugins: [
                  [transformCommonJS, {
                    warn: console.warn,
                    error: console.error,              
                    warnOnDynamicExports: true,
                    warnOnDynamicRequire: true,
                    warnOnDynamicModule: true
                  }]
                ]
              }
            }
          }
        ]
    }
}