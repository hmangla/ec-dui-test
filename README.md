# ec-dui-test

### About
- This is the test repository of ec-dui which helps to get the basic understanding of ec-dui functioning
- JOS is being used to serve the ec-dui microapp through iframe

### To Setup
git clone --recursive https://bitbucket.org/hmangla/ec-dui-test.git

npm i && bower i

### To run
npm run start

Open http://0.0.0.0:3000/